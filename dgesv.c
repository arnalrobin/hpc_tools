#include <time.h>
#include <stdio.h>
#include <stdlib.h>

//#include "mkl_lapacke.h"

double absolute_value(double value){
    double a;
    a = (value >= 0)?value:-value;
    return a;
}

int check_result(double *bref, double *b, int size) {
    int i;
    double epsilon;
    
    epsilon = 0.0005;
    for(i=0;i<size*size;i++) {
        if (absolute_value(bref[i] - b[i]) > epsilon ) return 0;
    }
    return 1;
}


double *generate_matrix(int size)
{
    int i;
    double *matrix = (double *)malloc(sizeof(double) * size * size);
    srand(1);

    for (i = 0; i < size * size; i++)
    {
        matrix[i] = rand() % 100;
    }

    return matrix;
}

double *generate_matID(int size){
    int i, j;
    double *matrix = (double *)malloc(sizeof(double) * size * size);

    for (i = 0; i < size; i++){
        for(j=0; j<size; j++){
            if(i == j){
                matrix[i * size + j]=1;
            }else{
                matrix[i * size + j] = 0;
            }
        }
    }
    return matrix;
}



void print_matrix(const char *name, double *matrix, int size)
{
    int i, j;
    printf("matrix: %s \n", name);

    for (i = 0; i < size; i++)
    {
            for (j = 0; j < size; j++)
            {
                printf("%f ", matrix[i * size + j]);
            }
            printf("\n");
    }
}



// Return the index of The maximum value of the column.
float getIndexMaxColonne(double *matrix, int columnIndex, int size, int r){
    double res;
    double max = 0;

    int i;
    for (i =r+1; i<size; i++){
        if (absolute_value(matrix[i * size + columnIndex]) >= max){
            max = absolute_value(matrix[i * size + columnIndex]);
            res = i;
        } 
    }
    return res;
}


// DIvide a row by a certain value
void divideRow(double* matrix, double *matID, int rowIndex, double value, int size){
    int j;

    for(j =0;j<size;j++){
        matrix[rowIndex * size + j] = matrix[rowIndex * size + j] / value;
        matID[rowIndex * size + j] = matID[rowIndex * size + j] / value;

    }

}

// Exchange the values of row Index1 and row Index2
void exchange(double * matrix, double *matID, int index1, int index2, int size){
    
  int i ; //Variable d'itération
double * temporaryRow = malloc(size * sizeof(double));
  //
  for (i = 0; i < size; ++i) {
    temporaryRow[i] = matrix[index1 * size + i ];
    matrix[index1 * size + i] = matrix[index2 * size + i];
    matrix[index2 * size + i] = temporaryRow[i];
  }

    for (i = 0; i < size; ++i) {
    temporaryRow[i] = matID[index1 * size + i ];
    matID[index1 * size + i] = matID[index2 * size + i];
    matID[index2 * size + i] = temporaryRow[i];
  }

    free(temporaryRow);
}

// Function that multiply the row 'RowIndex2' by a certain value. Then substract this result to the row 'rowIndex1'
void substractTwoRows(double *matrix, double *matID, int rowIndex1, int rowIndex2, double value, int size){

    int j;
    #pragma omp parallel for simd schedule(simd : static)
    for(j=0;j<size;j++){
        matrix[rowIndex1 * size + j] = matrix[rowIndex1 * size + j] - (value * matrix[rowIndex2 * size + j]);
        matID[rowIndex1 * size + j] = matID[rowIndex1 * size + j] - (value * matID[rowIndex2 * size + j]);
    }

}




// Finale function to do the Gauss-Jordan method.
// after this function 'matrix' will be an identity matrix and the inverse of A is store in matID
void gauss_jordan(double* matrix, double *matID, int size){
    int r = -1;
    int i, j, k;

    for(j=0;j<size;j++){
        k = getIndexMaxColonne(matrix, j,size, r );
        if(matrix[k * size + j] != 0){
            r += 1;
            divideRow(matrix,matID, k,matrix[k * size + j], size );
            exchange(matrix, matID, k,r,size);
            for(i=0;i<size;i++){
                if(i != r){
                    substractTwoRows(matrix, matID, i ,r, matrix[i * size +j],  size);
                }
            }
        }
    }
}

// Multiply two matrices
void * multiplyMat(double *matrix1, double *matrix2, double *resultMatrix, int size){
    int i, j, k;

    #pragma omp parallel for private(j,k) schedule(dynamic)
    for(i=0; i < size ; i++){
        for(j = 0 ; j < size ; j++){
            resultMatrix[i * size + j] = 0 ;
            for(k = 0 ; k < size ; k ++){
                resultMatrix[ i * size + j] += matrix1[i * size + k] * matrix2[k * size + j];
            }
        }
    }
}

void my_dgesv(double * A, double *matID, double *B, double * X, int size){

    //Apply guass_jordan algorithme, after this function, A is now a identity matrix and the inverse matrix of A is stored in matID
    gauss_jordan(A, matID, size);


    // We multiply the inverse of A with B to have our result. This result is stored in matrix X
    multiplyMat(matID, B, X, size);
}


    void main(int argc, char *argv[])
    {

        int size = atoi(argv[1]);

        double *a, *aref;
        double *b, *bref;
        double *matID;
        double *X;

        a = generate_matrix(size);
        aref = generate_matrix(size);        
        b = generate_matrix(size);
        bref = generate_matrix(size);

        matID = generate_matID(size);
        X = generate_matrix(size);

        //print_matrix("A", a, size);
        //print_matrix("B", b, size);

        // Using MKL to solve the system
        // MKL_INT n = size, nrhs = size, lda = size, ldb = size, info;
        // MKL_INT *ipiv = (MKL_INT *)malloc(sizeof(MKL_INT)*size);

        // double tStart=MPI_Wtime();
        // info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, aref, lda, ipiv, bref, ldb);
        // printf("Time taken by MKL: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

        // MKL_INT *ipiv2 = (MKL_INT *)malloc(sizeof(MKL_INT)*size);        
        my_dgesv(a,matID, b, X, size);
        // printf("Time taken by my implementation: %.2fs\n",(MPI_Wtime() - tStart));
	    // print_matrix("A",a,size);
	// print_matrix("matID",matID,size);
	//print_matrix("X",X,size);
	//print_matrix("bres",bref,size);
        if (check_result(bref,X,size)==1)
            printf("Result is ok!\n");
        else    
            printf("Result is wrong!\n");
        
        //print_matrix("X", b, size);
        //print_matrix("Xref", bref, size);
        free(aref);
        free(b);
        free(bref);
        free(matID);
        free(X);
        free(a);

        exit(0);
    }